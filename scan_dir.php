<?php

class Scan_Dir{

    public function __construct(){
    }

    public function scan_dir($dir) {
        $ignored = array('.', '..');
        $files = array();    
		$files = scandir($dir);
		
		//$files = preg_grep("/^(?!.*\.temp).*$/i", $files); // ignore *.temp files
		$template = preg_match('/^*$/', $file);
		
        foreach ($files as $file) {
            if (in_array($file, $ignored) || !$template) continue;
			
            if ($template) {
				$hash = filesize($dir . '/' . $file) + crc32($file);
			}
			elseif (filesize($dir . '/' . $file) > 10 && $template) {
				$hash = filesize($dir . '/' . $file) + filemtime($dir . '/' . $file) + crc32($file);
			}
			else{
				continue;
			}
            $files[$file] = $hash;
        }
        return ($files) ? $files : false;
    }
}
